package HttpClient

import java.util.concurrent.TimeUnit

import Data.{Comment, User}
import akka.http.scaladsl.Http
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import com.github.tomakehurst.wiremock.matching.{ContentPattern, StringValuePattern}
import org.json4s.jackson.Serialization

//import dispatch.{ Http, url }


class WireMockTests extends FlatSpec with Matchers with BeforeAndAfterEach {
  private val port = 8080
  private val hostname = "localhost"

  val httpClient = new HttpClient
  //val uri = "https://jsonplaceholder.typicode.com/"

  private val wireMockServer = new WireMockServer(wireMockConfig().port(port))

  override def beforeEach {
    wireMockServer.start()
  }

  override def afterEach {
    wireMockServer.stop()
  }

  val uri = "https://jsonplaceholder.typicode.com/"

  val response = Comment(1, 1, "id labore ex et quam laborum",
    "Eliseo@gardner.biz",
    "laudantium enim quasi est quidem magnam voluptate ipsam eos\n" +
      "tempora quo necessitatibus\n" +
      "dolor quam autem quasi\n" +
      "reiciendis et nam sapiente accusantium")

  "Your Client" should "send proper request" in {
      val path = uri + s"/comments/1"
      // Configure mock server stub response
      // json4s is useful to constructing response string if the response is JSON
      wireMockServer.stubFor(
        get(urlPathEqualTo(path))
          .willReturn(aResponse()
            .withHeader("Content-Type", "application/json")
            .withStatus(200)))

      // Send request by using your HTTP client
    //httpClient.get[Comment](uri + s"comments/1")

      // Verify the request is valid
      /*wireMockServer.verify(
        getRequestedFor(urlPathEqualTo(path))
          .withHeader("Content-Type", "application/json")) */
    }

    // Assert response body itself if necessary
}
