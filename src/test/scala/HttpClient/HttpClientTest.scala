package HttpClient

import Data._
import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.Matchers._
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures

class HttpClientTest extends TestKit(ActorSystem()) with Json4sSupport with AsyncFlatSpecLike with ScalaFutures {

  val httpClient = new HttpClient
  val uri = "https://jsonplaceholder.typicode.com/"

  it should "get list of users" in {
    httpClient.get[User](uri + s"users").map(list => list.size shouldBe 10)
  }

  it should "get list of Albums of User" in {
    httpClient.get[Album](uri + s"albums?userId=1").map(_.length shouldBe 10)
  }

  it should "get list of Comments of Post" in {
    httpClient.get[Comment](uri + s"comments?postId=1").map(_.length shouldBe 5)
  }

  it should "get list of Posts of User" in {
    httpClient.get[Post](uri + s"posts?userId=1").map(_.length shouldBe 10)
  }

  it should "get list of Photos of Album" in {
    httpClient.get[Photo](uri + s"photos?albumId=1").map(_.length shouldBe 50)
  }

  it should "get list of Todos of User" in {
    httpClient.get[Todo](uri + s"todos?userId=1").map(_.length shouldBe 20)
  }

  val comment = Comment(1, 1, "id labore ex et quam laborum",
    "Eliseo@gardner.biz",
    "laudantium enim quasi est quidem magnam voluptate ipsam eos\n" +
      "tempora quo necessitatibus\n" +
      "dolor quam autem quasi\n" +
      "reiciendis et nam sapiente accusantium")

  it should "get comment by id" in {
    httpClient.getById[Comment](uri + s"comments/1").map(_ shouldBe comment)
  }

  val album = Album(1, 1,"Test")

  it should "post new Album" in {
    httpClient.post[Album](uri + s"albums", album).map(result => {
      result.userId shouldBe 1
      result.title shouldBe "Test"
    })
  }

  val photo = Photo(1, 1, "Test", "Test", "Test")

  it should "put new Photo" in {
    httpClient.put[Photo](uri + s"photos/1", photo).map(result =>{
      result.albumId shouldBe 1
      result.id shouldBe 1
      result.title shouldBe "Test"
      result.url shouldBe "Test"
      result.thumbnailUrl shouldBe "Test"
    })
  }

  val post = Post(1, 1, "Test", "Test")

  it should "update Post" in {
    httpClient.patch(uri + s"posts/1", post).map(result => {
      result.userId shouldBe 1
      result.title shouldBe "Test"
      result.body shouldBe "Test"

    })
  }

  it should "delete Todo" in {
    httpClient.delete(uri + "todos/1").map(_.status.isSuccess() shouldBe true)
  }
}
