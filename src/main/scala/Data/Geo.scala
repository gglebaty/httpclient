package Data

case class Geo (lat: String,
                lng: String)
