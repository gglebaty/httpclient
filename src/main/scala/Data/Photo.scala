package Data

case class Photo(albumId: Int,
            id: Int,
            title: String,
            url: String,
            thumbnailUrl: String)

