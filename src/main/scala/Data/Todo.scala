package Data

case class Todo(userId: Int,
           id: Int,
           title: String,
           completed: Boolean)
