package Data

case class Address(street: String,
              suite: String,
              city: String,
              zipcode: String,
              geo: Geo
             )
