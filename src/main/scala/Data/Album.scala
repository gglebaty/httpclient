package Data

case class Album(userId: Int,
            id: Int,
            title: String)
