package Data


case class User(id: Int,
           name: String,
           username: String,
           email: String,
           address: Address,
           phone: String,
           website: String,
           company: Company)

