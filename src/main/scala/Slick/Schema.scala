package Slick

import Data._
import slick.jdbc.H2Profile.api._
import scala.concurrent.Future

object Schema {

  class Albums(tag: Tag) extends Table[Album](tag, "ALBUMS"){
    def userId: Rep[Int] = column("USER_ID")
    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def title: Rep[String] = column("TITLE")

    def * = (userId, id, title) <> (Album.tupled, Album.unapply)

    def user = foreignKey("USER_FK", userId, users)(_.id)
  }

  val albums = TableQuery[Albums]

  class Photos(tag: Tag) extends Table[Photo](tag, "PHOTOS"){
    def albumId: Rep[Int] = column("ALBUM_ID")
    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def title: Rep[String] = column("TITLE")
    def url: Rep[String] = column("URL")
    def thumbnailUrl: Rep[String] = column("THUMBNAIL_URL")

    def * = (albumId, id, title, url, thumbnailUrl) <> (Photo.tupled, Photo.unapply)

    def album = foreignKey("ALBUM_FK", albumId, albums)(_.id)
  }

  val photos = TableQuery[Photos]

  class Posts(tag: Tag) extends Table[Post](tag, "POSTS"){
    def userId: Rep[Int] = column("USER_ID")
    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def title: Rep[String] = column("TITLE")
    def body: Rep[String] = column("BODY")

    def * = (userId, id, title, body) <> (Post.tupled, Post.unapply)

    def user = foreignKey("USER_FK", userId, users)(_.id)
  }

  val posts = TableQuery[Posts]

  class Comments(tag: Tag) extends Table[Comment](tag: Tag, "COMMENTS"){
    def postId: Rep[Int] = column("POST_ID")
    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def name: Rep[String] = column("NAME")
    def email: Rep[String] = column("EMAIL")
    def body: Rep[String] = column("BODY")

    def * = (postId, id, name, email, body) <> (Comment.tupled, Comment.unapply)

    def post = foreignKey("POST_FK", postId, posts)(_.id)
  }

  val comments = TableQuery[Comments]

  class Todos(tag: Tag) extends Table[Todo](tag: Tag, "TODOS"){
    def userId: Rep[Int] = column("USER_ID")
    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def title: Rep[String] = column("TITLE")
    def completed: Rep[Boolean] = column("COMPLETED")

    def * = (userId, id, title, completed) <> (Todo.tupled, Todo.unapply)

    def user = foreignKey("", userId, users)(_.id)
  }

  val todos = TableQuery[Todos]

  class Users(tag: Tag) extends Table[User](tag: Tag, "USERS"){

    def lat: Rep[String] = column("LAT")
    def lnt: Rep[String] = column("LNT")

    def geo = (lat, lnt)<>(Geo.tupled, Geo.unapply)

    def street: Rep[String] = column("STREET")
    def suite: Rep[String] = column("SUITE")
    def city: Rep[String] = column("CITY")
    def zipcode: Rep[String] = column("ZIPCODE")

    def address = (street, suite, city, zipcode, geo)<>(Address.tupled, Address.unapply)

    def companyName: Rep[String] = column("COMPANY_NAME")
    def catchPhrase: Rep[String] = column("CATCH_PHRASE")
    def bs: Rep[String] = column("BS")

    def company = (companyName, catchPhrase, bs)<>(Company.tupled, Company.unapply)


    def id: Rep[Int] = column("ID", O.PrimaryKey, O.AutoInc)
    def name: Rep[String] = column("STRING")
    def username: Rep[String] = column("USERNAME")
    def email: Rep[String] = column("EMAIL")
    def phone: Rep[String] = column("PHONE")
    def website: Rep[String] = column("WEBSITE")

    def * = (id, name, username, email, address, phone, website, company) <> (User.tupled, User.unapply)
  }

  val users = TableQuery[Users]

  val db = Database.forConfig("h2mem1")

  def setup(): Future[Unit] = db.run((Schema.albums.schema ++
    Schema.photos.schema ++
    Schema.posts.schema ++
    Schema.comments.schema ++
    Schema.todos.schema ++
    Schema.users.schema).create)

}
