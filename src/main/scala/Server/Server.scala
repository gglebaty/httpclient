package Server

import akka.actor.ActorSystem
import akka.http.scaladsl.{Http, server}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import de.heikoseeberger.akkahttpjson4s.Json4sSupport._
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization

object Server {
  implicit val serialization = Serialization
  implicit val format = DefaultFormats
  implicit val system: ActorSystem = ActorSystem()
  implicit val mat: ActorMaterializer = ActorMaterializer()

  def main(args: Array[String]): Unit = {

    val bindingFuture = Http().bindAndHandle(routes(), "localhost", 8080)

    println(s"Server online at http://localhost:8080/")

  }

  /*path(s"albums&userId=$IntNumber"){ userId =>
    get {
      complete(s"$userId")
    }
  } ~*/
  //repository: inMemoryRepository
  def routes():server.Route =  path(s"albums/$IntNumber"){
    get {
      complete("")
    }
    put {
      complete("")
    }
    patch {
      complete("")
    }
    delete {
      complete("")
    }
  }~ path(s"albums") {
    get {
      complete("albums")
    }
    post {
      complete("albums")
    }
  } ~ path(s"photos") {
    get {
      complete("photos")
    }
  } ~ path (s"posts"){
    get {
      complete("Posts")
    }
  } ~ path("comments") {
    get {
      complete("Welcome page")
    }
  } ~ path(s"todos") {
    get {
      complete("Todos")
    }
  } ~ path(s"users") {
    complete("users")
  }
}
