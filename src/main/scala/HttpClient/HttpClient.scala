package HttpClient

import Implicits._
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.{Marshal, Marshaller}
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling._

import scala.concurrent.Future

class HttpClient extends Json4sSupport {

  def get[T](uri: Uri)(implicit e: Unmarshaller[HttpResponse, List[T]]): Future[List[T]] = {
    val request = HttpRequest(HttpMethods.GET, uri)
    Http().singleRequest(request)
      .flatMap(Unmarshal(_).to[List[T]])
  }

  def getById[T](uri: Uri)(implicit e: Unmarshaller[HttpEntity, T]): Future[T] = {
    val request = HttpRequest(HttpMethods.GET, uri)
    Http().singleRequest(request)
      .flatMap(Unmarshal(_).to[T])
  }

  def post[T](uri: Uri, entity: T)(implicit e: Unmarshaller[HttpEntity, T], m: Marshaller[T, RequestEntity]): Future[T] = {
    Marshal(entity).to[RequestEntity].flatMap{ value =>
      Http().singleRequest(HttpRequest(HttpMethods.POST,
        uri,
        entity = value
      )).flatMap(Unmarshal(_).to[T])
    }
  }


  def put[T](uri: Uri, entity: T)(implicit e: Unmarshaller[HttpEntity, T], m: Marshaller[T, RequestEntity]): Future[T] = {
    Marshal(entity).to[RequestEntity].flatMap{ value =>
      Http().singleRequest(HttpRequest(HttpMethods.PUT,
        uri,
        entity = value
      )).flatMap(Unmarshal(_).to[T])
    }
  }

  def patch[T](uri: Uri, entity: T)(implicit e: Unmarshaller[HttpEntity, T], m: Marshaller[T, RequestEntity]): Future[T] = {
    Marshal(entity).to[RequestEntity].flatMap{ value =>
      Http().singleRequest(HttpRequest(HttpMethods.PATCH,
        uri,
        entity = value
      )).flatMap(Unmarshal(_).to[T])
    }
  }

  def delete(uri: Uri): Future[HttpResponse] = {
    val request = HttpRequest(
      HttpMethods.DELETE,
      uri
    )
    Http().singleRequest(request)
  }
}


